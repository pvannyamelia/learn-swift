import UIKit

/*-----------------------------------------------------------------------
 If conditionals:
  * to combine use || and &&
 ------------------------------------------------------------------------*/
var bil1 = 3
var bil2 = 2

if bil1 > bil2 {
    print("Bilangan pertama paling besar")
} else if bil1 < bil2 {
    print("Bilangan kedua paling besar")
} else {
    print("Sama Besar")
}

// Ternary If
print(bil1 == bil2 ? "bilangan sama" : "bilangan beda")


/*-----------------------------------------------------------------------
 Switch conditionals
 ------------------------------------------------------------------------*/
var weather = "sunny"

switch weather {
case "rainy":
    print("It's rainy today")
case "sunny":
    print("It's sunny")
    fallthrough //untuk cek juga setelah setelahnya
default:
    print("Have a nice day!")
}

/*-----------------------------------------------------------------------
 Range
 ..< creates ranges up but exclude final value
 ... creates ranges up including final value
 ------------------------------------------------------------------------*/
for i in 1..<5 {
    print(i)
}
for i in 1...5 {
    print(i)
}
