
import UIKit

/*-----------------------------------------------------------------------
 Declaring Variable
 ------------------------------------------------------------------------*/
var str = """
Kezia Karen Amelia is my name \
I was born in Malang
"""

/*-----------------------------------------------------------------------
 Inserting variable into string
 ------------------------------------------------------------------------*/
var comb = "I am \(16) years old"

/*-----------------------------------------------------------------------
 Declaring Constant (Let)
 ------------------------------------------------------------------------*/
let string = "this is a constant"

/*-----------------------------------------------------------------------
 Type Annotations
 ------------------------------------------------------------------------*/
var title: String = "Reputation"
print(Int("5"))

/*-----------------------------------------------------------------------
 Array
 ------------------------------------------------------------------------*/
var array : Any = ["Kezia Karen", 16, 1.55] //Declaring array with different types
var emptyArr = Array<Int>()
// var emptyArr = [Int]() // Cara lain

/*-----------------------------------------------------------------------
 Set & Tuple
 ------------------------------------------------------------------------*/
var set = Set(["Red", "Green", "Blue", "Blue"]) // Set hanya bisa 1 tipe, urutannya random, ga bisa dobel
var emptySet = Set<String>()

var name = (first: "Justin", age: 25) // tuple, immutable
name.1
name.first


/* ------------------------------------------------------------------------
Dictionaries
---------------------------------------------------------------------------*/
var regularDict = [
    "name": "Priscilla Vanny",
    "age": "20",
    "height": "1.63"
]

//var bio = Dictionary<String, Any>() // Dictionary with different data types
 var bio = [String: Any]()
bio["name"] = "Priscilla Vanny"
bio["age"] = 20
print(bio["name"]!)

var dictDefault = [
    "Kezia": "Chocolate",
    "Priscilla": "Strawberry"
]
dictDefault["Dimitri",
            default: "Vanilla"]


/*-----------------------------------------------------------------------
 Enums: stops you from accidentally using different strings each time. grouping related values so you can use them without spelling mistakes.
 ------------------------------------------------------------------------*/
enum Result {
    case Success
    case Failure
}
Result.Success

// Enum associated value
enum Activities {
    case bored
    case talking(topic: String)
}
let talking = Activities.talking(topic: "Podcast")

// Enum raw values: raw values itu semacam index, starting from 0, kecuali kita definiskan urutannya
enum Planets: Int {
    case mercury = 1
    case venus
    case earth
    case mars
}
let selectedPlanet = Planets(rawValue: 2)
