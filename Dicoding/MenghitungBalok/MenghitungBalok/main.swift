//
//  main.swift
//  MenghitungBalok
//
//  Created by Priscilla Vanny Amelia on 20/06/21.
//

import Foundation

print("Selamat datang di Dicoding Academy!")

let width = 2.0
let height = 3.0
let length = 4.0

let volume = width * height * length
let surfaceArea = 2 * ((width * height) + (width * length) + (height * length))
let circumference = 4 * (width + length + height)
print("---------------------")

print("Anda memiliki sebuah balok dengan:")
print("1. Lebarnya adalah \(width) cm.")
print("2. Tingginya adalah \(height) cm.")
print("3. Panjangnya adalah \(length) cm.")
print("4. Volumenya adalah \(volume) cm3.")
print("5. Luas permukaannya adalah \(surfaceArea) cm2.")
print("6. Kelilingnya adalah \(circumference) cm.")
print("----------------------------------")

print("Masukkanlah lebar balok:"); let widthInput = readLine()
print("Masukkanlah tinggi balok:"); let heightInput = readLine()
print("Masukkanlah panjang balok:"); let lengthInput = readLine()

if let length = Double(lengthInput ?? "0"), let height = Double(heightInput ?? "0"), let width = Double(widthInput ?? "0") {
    let volume = length * height * width
    let surfaceArea = 2 * ((width * length) + (width * height) + (height * length))
    let circumference = 4 * (width + length + height)
    print("----------------------------------")
  
    print("Anda memiliki sebuah balok dengan:")
    print("1. Lebarnya adalah \(width) cm.")
    print("2. Tingginya adalah \(height) cm.")
    print("3. Panjangnya adalah \(length) cm.")
    print("4. Volemenya adalah \(volume) cm3.")
    print("5. Luas permukaannya adalah \(surfaceArea) cm2.")
    print("6. Kelilingnya adalah \(circumference) cm.")
    print("----------------------------------")
} else {
    print("----------------------------------")
    print("Input tidak valid!")
    print("----------------------------------")
}
