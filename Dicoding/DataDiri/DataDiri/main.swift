//
//  main.swift
//  DataDiri
//
//  Created by Priscilla Vanny Amelia on 05/06/21.
//

import Foundation

print("Selamat Datang di Dicoding Academy")

print("Masukkan nama depan Anda:"); let firstName = readLine()!
print("Masukkan nama belakang Anda:"); let lastName = readLine()!
print("Masukkan umur Anda:"); let age = readLine()!
print("Masukkan alamat Anda"); let address = readLine()!
print("Masukkan pekerjaan Anda:"); let job = readLine()!

let fullName = firstName+" "+lastName

/*
let firstName = "Gilang", lastName = "Ramadhan"
let fullName = firstName+" "+lastName
let address = "Bandung"
let job = "iOS Developer"
let age = 0b10001
 */
print("---------------------------")
print("Apakah kalian tahu \(fullName)?")
print("\(firstName) adalah seorang \(job)")
print("Saat ini ia berumur \(age) dan bertempat tinggal di \(address)")

